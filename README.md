# alpinelinux-packages-demo

## Popular packages
* https://google.com/search?q=site%3Apkgs.alpinelinux.org

## Official documentation
### Alpine installation
* [*How to get regular stuff working*
  ](https://wiki.alpinelinux.org/wiki/How_to_get_regular_stuff_working)
* [bashell/alpine-bash](https://hub.docker.com/r/bashell/alpine-bash/dockerfile)